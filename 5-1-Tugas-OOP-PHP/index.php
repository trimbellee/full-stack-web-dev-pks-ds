<?php

//hewan

abstract class hewan
{
    protected $nama;
    public $darah =50;
    protected $jumlahKaki;
    protected $keahlian;

   

    protected function get_atraksi()
    {
        return $this->nama . ' sedang ' . $this->keahlian;
    }

}

//fight

abstract class fight extends hewan
{
    protected $attackPower;
    protected $defencePower;
    

    protected function serang(){
        return $this->nama . ' sedang menyerang ';
        
    }

    protected function diserang()
    {
        return ' sedang diserang ' . $this->nama;

    }
  
}

//elang

class elang extends fight
{
    public function tampil()
    {
        return 'nama : ' . $this->nama . ', Jumlah kaki : ' . $this->jumlahKaki . ', Keahlian : ' .
        $this->keahlian . ', Attack power : ' . $this->attackPower . ', Defence power : ' . $this->defencePower;
    }
    public function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $defencePower )
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian= $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower= $defencePower;
       
    }

    function get_atraksi()
    {
       return parent::get_atraksi();
    }
    function serang()
    {
        return parent::serang() . ' harimau. <br>' . 'harimau ' . parent::diserang() .'.<br>';
       
    }
    public function getInfoHewan()
    {
        return 'Nama : '. $this->nama . ', jumlah kaki : ' . $this->jumlahKaki . ', Darah :' . $this->darah . ', keahlian ' 
        . $this->keahlian . ', attack power :' . $this->attackPower . ', defence power :' . $this->defencePower;
    }
    
    
}

//harimau

class harimau extends fight
{
    public function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $defencePower )
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian= $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower= $defencePower;
       
    }
    public function tampil()
    {
        return 'nama : ' . $this->nama . ', Jumlah kaki : ' . $this->jumlahKaki . ', Keahlian : ' .
        $this->keahlian . ', Attack power : ' . $this->attackPower . ', Defence power : ' . $this->defencePower;
    }
    function get_atraksi()
    {
       return parent::get_atraksi();
    }
    function serang()
    {
        return parent::serang() . ' elang. <br>' . 'elang '. parent::diserang() . '.<br>';
     
    }
    
    public function getInfoHewan()
    {
        return 'Nama : '. $this->nama . ', jumlah kaki : ' . $this->jumlahKaki . ', Darah :' . $this->darah . ', keahlian ' 
        . $this->keahlian . ', attack power :' . $this->attackPower . ', defence power :' . $this->defencePower;

    }
        
}
// 'harimau', 4, 'lari cepat', 9, 9
$elang = new elang('elang', 2, 'terbang tinggi', 10, 5);
$harimau = new harimau('harimau', 4, 'lari cepat', 7, 8);
echo $elang->tampil();
echo '<br>';
echo $harimau->tampil();
echo '<br>';
echo $harimau->get_atraksi();
echo '<br>';
echo $elang->get_atraksi();
echo '<br>';
echo $elang->serang();
echo $harimau->serang();
echo $elang->getInfoHewan();
echo '<br>';
echo $harimau->getInfoHewan();


?>