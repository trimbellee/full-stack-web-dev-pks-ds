<?php

namespace App;

use App\roles;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable   = ['username' , 'email' , 'nama' , 'role_id' , 'password' , 'email_verified_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $primaryKey = 'id';
    protected $keyType    = 'string';
    public $incrementing  = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function($model)
        {
            if(empty($model->{$model->getKeyName()}))
            {
                $model->{$model->getKeyName()} = Str::uuid();
            }

            $model->role_id = roles::where('nama' , 'Author')->first()->id;
        });
    }

    public function roles()
    {
        return $this->belongsTo('App\roles');
    }
    public function otp_code()
    {
        return $this->hasOne('App\OtpCode');
    }

    // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function post()
    {
        return $this->hasMany('App\posts');
    }

    public function comment()
    {
        return $this->hasMany('App\comment');
    }
}
