<?php

namespace App\Http\Controllers;

use App\Events\PostCreated;
use App\User;
use App\posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index' , 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = posts::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'data daftar post berhasil di tampilkan',
            'data'    => $post
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestALl = $request->all();
        $validator = Validator::make($requestALl , [
                'judul' => 'required',
                'description' => 'required'
        ]);

        if($validator->fails())
        {       
            return Response()->json($validator->errors() , 400);
        };

        $post = posts::create($requestALl);

        event(new PostCreated($post));

        if($post)
        {
            return response()->json([
                'success' => true,
                'message' => 'data dengan judul : ' . $post->judul . ' berhasil di tambahkan',
                'data'    => $post
            ], 200);    
        }

            return Response()->json([
                'success' => false ,
                'message' => 'data dengan judul : ' . $post->judul . ' gagal di tambahkan'
            ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = posts::find($id);

        if($post)
        {
            return response()->json([
                'success' => true,
                'message' => 'Detail Data post',
                'data'    => $post 
            ], 200);
        }

        return Response()->json([
            'success' => false ,
            'message' => 'data dengan id : ' . $id . ' tidak ditemukan'
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all() , [
            'judul' => 'required',
            'description' => 'required'
        ]);

        if($validator->fails())
        {       
            return Response()->json($validator->errors() , 400);
        };

        $post = posts::findOrFail($id);


        if($post)
        {
            $user = auth()->user();

            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login'
                ], 403);
            }
            $post->update([
                'judul'     => $request->judul,
                'description' => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'data dengan judul : ' . $post->judul . ' berhasil di update',
                'data'    => $post  
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'post Not Found'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $post = posts::findOrfail($id);

        if ($post)
        {
            $user = auth()->user();

            if($post->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data post bukan milik user login'
                ], 403);
            }

                $post->delete();
                return response()->json([
                'success' => true,
                'message' => 'data dengan judul : ' . $post->judul . ' berhasil dihapus',
                ], 200);
        }
        
        return response()->json([
            'success' => false,
            'message' => 'post Not Found',
        ], 404);
            
        
    }
    
}
