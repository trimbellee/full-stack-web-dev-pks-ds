<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $requestAll = $request->all();
        $validator = Validator::make($requestAll , [
            'email'      => 'required',
            'password'     => 'required'
            
        ]);

        if($validator->fails())
        {       
            return Response()->json($validator->errors() , 400);
        };

        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json([
                'success'   => false,
                'message'   => 'email dan password tidak ditemukan'              
            ], 401);
        }

        return response()->json([
            'success'   => true,
            'message'   => 'user berhasil login',
            'data'      =>[
                'user'  => auth()->user(),
                'token' => $token
            ]              
        ]);

    }
}
