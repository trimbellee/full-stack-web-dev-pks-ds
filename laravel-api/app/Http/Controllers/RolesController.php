<?php

namespace App\Http\Controllers;

use App\roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = roles::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'data daftar roles berhasil di tampilkan',
            'data'    => $roles

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestALl = $request->all();
        $validator = Validator::make($requestALl , [
        'nama' => 'required'
            ]);

            if($validator->fails())
            {       
    
                return Response()->json($validator->errors() , 400);
            };

        $roles = roles::create($requestALl);
        if($roles)
        {
        
            return response()->json([
                'success' => true,
                'message' => 'data  roles berhasil di tambahkan',
                'data'    => $roles

            ], 200);    
        }

            return Response()->json([
                'success' => false ,
                'message' => 'data roles gagal dibuat'
            ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $roles = roles::findOrfail($id);

        return response()->json([
        'success' => true,
        'message' => 'Detail Data roles',
        'data'    => $roles 
        ], 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validator = Validator::make($request->all() , [
        'nama' => 'required'
        ]);

          if($validator->fails())
        {       
  
            return Response()->json($validator->errors() , 400);
        };

        $roles = roles::findOrFail($id);


         if($roles)
         {
            
            $roles->update([
                'nama'     => $request->nama
            ]);

            return response()->json([
                'success' => true,
                'message' => 'roles Updated',
                'data'    => $roles  
            ], 200);
         }

         return response()->json([
            'success' => false,
            'message' => 'roles Not Found'
        ], 404);
        


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $roles = roles::findOrfail($id);

        $roles->delete();

        
        if ($roles)
        {
            return response()->json([
            'success' => true,
            'message' => 'roles Deleted',
            ], 200);
        }
        
        return response()->json([
            'success' => false,
            'message' => 'roles Not Found',
        ], 404);
        
    }
    
}
