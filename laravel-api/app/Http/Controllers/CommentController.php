<?php

namespace App\Http\Controllers;

use App\comment;
use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index' , 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comment = comment::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'data daftar comment berhasil di tampilkan',
            'data'    => $comment

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $requestALl = $request->all();
        $validator = Validator::make($requestALl , [
                'content' => 'required',
                'post_id' => 'required'
            ]);

            if($validator->fails())
            {       
    
                return Response()->json($validator->errors() , 400);
            };

        $comment = comment::create($requestALl);
        
        Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));
          
        if($comment)
        {
        
            return response()->json([
                'success' => true,
                'message' => 'data  comment berhasil di tambahkan',
                'data'    => $comment

            ], 200);    
        }

            return Response()->json([
                'success' => false ,
                'message' => 'data comment gagal dibuat'
            ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = comment::findOrfail($id);

        if($comment)
        {
            return response()->json([
                'success' => true,
                'message' => 'Detail Data comment',
                'data'    => $comment 
            ], 200);
        }

        return Response()->json([
            'success' => false ,
            'message' => 'comment dengan id : ' . $id . ' tidak ditemukan'
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all() , [
            'content' => 'required'
        ]);
    
        if($validator->fails())
        {       
    
            return Response()->json($validator->errors() , 400);
        };
    
        $comment = comment::findOrFail($id);


        if($comment)
        {
            $user = auth()->user();

            if($comment->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data comment bukan milik user login'
                ], 403);
            }
            
                $comment->update([
                    'content'     => $request->content
                ]);

                return response()->json([
                    'success' => true,
                    'message' => 'comment Updated',
                    'data'    => $comment  
                ], 200);

            return response()->json([
            'success' => false,
            'message' => 'comment Not Found'
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    {
        $comment = comment::findOrfail($id);
       
        if ($comment)
        {
            $user = auth()->user();

            if($comment->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data comment bukan milik user login'
                ], 403);
            }

            $comment->delete();
            return response()->json([
            'success' => true,
            'message' => 'comment Deleted',
            ], 200);
        }
        
        return response()->json([
            'success' => false,
            'message' => 'comment Not Found',
        ], 404);
    }
    
}
