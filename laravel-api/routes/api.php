<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::apiResource('roles' , 'RolesController');
Route::apiResource('comment' , 'CommentController');
Route::apiResource('posts' , 'PostController');

Route::group([
    'prefix' => 'auth' ,
    'namespace' => 'Auth'
], function(){
    Route::post('register' , 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code' , 'RegenerateOtpCodeController')->name('auth.regenerateOtpCode');
    Route::post('verification' , 'VerificationController')->name('auth.verification');
    Route::post('update-password' , 'UpdatePasswordController')->name('auth.update-password');
    Route::post('login' , 'LoginController')->name('auth.login');
});
