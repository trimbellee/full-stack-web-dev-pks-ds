<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Aplikasi {{ $title }}</title>
</head>
<body>
    <p>Selamat datang {{ $title }} .Ini adalah kode OTP Anda : {{ $user->otp_code->otp }}. Kode OTP ini berlaku 5 menit. Jangan berikan kode ini kepada siapapun.</p>
</body>
</html>